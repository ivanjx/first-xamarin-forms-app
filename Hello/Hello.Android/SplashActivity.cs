﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;

namespace Hello.Droid
{
    [Activity(Label = "Hello", Icon = "@drawable/icon", Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        protected override void OnResume()
        {
            base.OnResume();
            StartupProcess();
        }

        public override void OnBackPressed()
        {
            // Do nothing to 'disable' the back button.
        }

        void StartupProcess()
        {
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
        }
    }
}