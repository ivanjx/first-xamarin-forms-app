﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Hello.Interfaces;
using Hello.Droid.Commons;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(NativeSample))] // Mandatory!
namespace Hello.Droid.Commons
{
    class NativeSample : INativeSample
    {
        public async Task<string> GetNativeString()
        {
            await Task.Delay(1000);

            StringBuilder sb = new StringBuilder();

            foreach (object folder in Enum.GetValues(typeof(System.Environment.SpecialFolder)))
            {
                sb.Append(folder).Append("=").Append(System.Environment.GetFolderPath((System.Environment.SpecialFolder)folder)).Append(System.Environment.NewLine);
            }

            return sb.ToString();
        }
    }
}