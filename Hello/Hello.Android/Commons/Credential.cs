﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Hello.Interfaces;
using Hello.Droid.Commons;
using Xamarin.Auth;
using Xamarin.Forms;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(Credential))]
namespace Hello.Droid.Commons
{
    class Credential : ICredential
    {
        public async Task<string> GetTokenAsync()
        {
            Account account = (await AccountStore.Create(Forms.Context).FindAccountsForServiceAsync(Hello.Commons.App.AppName)).FirstOrDefault();
            if (account == null)
            {
                return null;
            }
            else
            {
                return account.Properties["token"];
            }
        }

        public async Task DeleteCredentialAsync()
        {
            Account account = (await AccountStore.Create(Forms.Context).FindAccountsForServiceAsync(Hello.Commons.App.AppName)).FirstOrDefault();
            if (account != null)
            {
                await AccountStore.Create().DeleteAsync(account, Hello.Commons.App.AppName);
            }
        }

        public async Task SaveCredentialAsync(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentNullException("token");
            }

            Account account = new Account();
            account.Properties.Add("token", token);
            await AccountStore.Create().SaveAsync(account, Hello.Commons.App.AppName);
        }
    }
}