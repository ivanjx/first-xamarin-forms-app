﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello.Commons
{
    public static class Authenticator
    {
        public static async Task<string> LoginAsync(string username, string password)
        {
            await Task.Delay(3000); // Simulate long running task.
            if (username == "codexana133@gmail.com" && password == "12345678")
            {
                return "9802938409820934890JNIDJOIUWHiehwiuehqwe";
            }
            else
            {
                return null;
            }
        }

        public static async Task LogoutAsync(string token)
        {
            await Task.Delay(3000);
        }

        public static async Task<bool> CheckTokenAsync(string token)
        {
            // Simulate long running task.
            await Task.Delay(3000);
            return true;
        }
    }
}
