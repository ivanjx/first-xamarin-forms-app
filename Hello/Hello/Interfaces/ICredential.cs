﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello.Interfaces
{
    public interface ICredential
    {
        Task<string> GetTokenAsync();
        Task SaveCredentialAsync(string token);
        Task DeleteCredentialAsync();
    }
}
