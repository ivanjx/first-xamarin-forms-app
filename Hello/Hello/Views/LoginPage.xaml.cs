﻿using Hello.Commons;
using Hello.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hello
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        Application m_app;


        public LoginPage(Application app)
        {
            InitializeComponent();
            m_app = app;

            // Setting the navigation titlebar to invisible.
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private async void btnLogin_Clicked(object sender, EventArgs e)
        {
            loadingIndicator.IsVisible = true;
            loadingIndicator.IsRunning = true;
            btnLogin.IsEnabled = false;

            string email = txtEmail.Text;
            string password = txtPassword.Text;

            try
            {
                // Performing dummy task.
                await Task.Delay(3000);

                // Verifying.
                string token;
                if (!string.IsNullOrEmpty((token = await Authenticator.LoginAsync(email, password))))
                {
                    // Saving token.
                    ICredential credential = DependencyService.Get<ICredential>();
                    await credential.SaveCredentialAsync(token);

                    // Navigating to main page.
                    m_app.MainPage = new NavigationPage(new MainPage(m_app, token));
                }
                else
                {
                    await DisplayAlert("Login Error", "Invalid email and password combination!", "OK");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error occurred.\nDetails: " + ex.Message, "OK");
            }
            finally
            {
                loadingIndicator.IsVisible = false;
                loadingIndicator.IsRunning = false;
                btnLogin.IsEnabled = true;
            }
        }
    }
}