﻿using Hello.Commons;
using Hello.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hello
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        Application m_app;
        string m_token;


        public MainPage(Application app, string token)
        {
            InitializeComponent();
            m_app = app;
            m_token = token;

            // Setting the navigation titlebar to invisible.
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private async void btnAbout_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AboutPage());
        }

        private async void btnLogout_Clicked(object sender, EventArgs e)
        {
            // Showing the loading page.
            m_app.MainPage = new LoadingPage();

            // Logging out.
            await Authenticator.LogoutAsync(m_token);

            // Deleting session.
            ICredential credential = DependencyService.Get<ICredential>();
            await credential.DeleteCredentialAsync();

            // Redirecting to the login page.
            m_app.MainPage = new LoginPage(m_app);
        }
    }
}