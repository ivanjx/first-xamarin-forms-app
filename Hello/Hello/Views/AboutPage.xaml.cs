﻿using Hello.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hello
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();

            // Setting the navigation titlebar to invisible.
            NavigationPage.SetHasNavigationBar(this, false);

            // Testing the native function.
            ShowNativeString();
        }

        async void ShowNativeString()
        {
            string nativeString = await DependencyService.Get<INativeSample>().GetNativeString();
            await DisplayAlert("Native String", nativeString, "OK");
        }
    }
}