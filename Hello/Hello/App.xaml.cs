﻿using Hello.Commons;
using Hello.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Hello
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // Showing the loading animation.
            MainPage = new LoadingPage();
            
            Init();
        }

        async void Init()
        {
            // Checking stored credentials.
            ICredential credential = DependencyService.Get<ICredential>();
            string token = await credential.GetTokenAsync();

            if (string.IsNullOrEmpty(token))
            {
                await credential.DeleteCredentialAsync();
                MainPage = new LoginPage(this);
            }
            else
            {
                if (await Authenticator.CheckTokenAsync(token))
                {
                    MainPage = new NavigationPage(new MainPage(this, token));
                }
                else
                {
                    MainPage = new LoginPage(this); // Authentication failed. Relogin!
                }
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
